package com.myoffer.trendingbanglajokes;

/**
 * Created by TahmiD on 5/8/2016.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "jokesInfo";
    // Contacts table name
    private static final String TABLE_JOKES = "jokes";
    // Jokes Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_CATEGORY = "joke_category";
    private static final String KEY_TITLE = "joke_title";
    private static final String KEY_BODY = "joke_body";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_JOKES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CATEGORY + " TEXT,"
                + KEY_TITLE + " TEXT," + KEY_BODY + " TEXT" + " )";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOKES);
// Creating tables again
        onCreate(db);
    }
    // Adding new joke
    public void addJoke(Joke joke) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, joke.getJokeCategory()); // Joke Name
        values.put(KEY_TITLE, joke.getJokeTitle()); // Joke Phone Number
        String[] interm = joke.getJokeBody().split("\\.") ;
        String jokebody="" ;
        for(int i=0;i<interm.length;i++)
            jokebody+=interm[i] + "\n" ;
        values.put(KEY_BODY, jokebody);
// Inserting Row
        db.insert(TABLE_JOKES, null, values);
        db.close(); // Closing database connection
    }



    // Getting one joke
    public Joke getJoke(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_JOKES, new String[]{KEY_ID,
                        KEY_CATEGORY, KEY_TITLE,KEY_BODY}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Joke contact = new Joke(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2),cursor.getString(3));
// return joke
        return contact;
    }


    public String readAllJokes(StringBuilder stringBuilder){
        String selectQuery = "SELECT * FROM " + TABLE_JOKES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do{
                stringBuilder.append(cursor.getString(0) + " ") ;
                stringBuilder.append(cursor.getString(1) + " ") ;
                stringBuilder.append(cursor.getString(2) + " ") ;
                stringBuilder.append(cursor.getString(3) + " ") ;
                stringBuilder.append("\n") ;
            }while(cursor.moveToNext()) ;
        }
        return stringBuilder.toString() ;
    }
    public String getBody(String headline){
        String body ="" ;
        String selectQuery = "SELECT  DISTINCT "+KEY_BODY +" FROM " + TABLE_JOKES + " WHERE " + KEY_TITLE + " = '" + headline+"'" ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                body =cursor.getString(0);
            } while (cursor.moveToNext());
        }
        return body ;
    }
    public ArrayList<String> getAllHeadlines(String category){
        ArrayList<String> categoryList= new ArrayList<String>() ;
        String selectQuery = "SELECT  DISTINCT "+KEY_TITLE +" FROM " + TABLE_JOKES + " WHERE " + KEY_CATEGORY + " = '" + category+"'" ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                categoryList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return categoryList;
    }
    public ArrayList<String> getAllCategory(){
        ArrayList<String> categoryList= new ArrayList<String>() ;
        String selectQuery = "SELECT  DISTINCT "+KEY_CATEGORY +" FROM " + TABLE_JOKES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                categoryList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return categoryList;
    }

    public boolean isEmpty()
    {
        String selectQuery = "SELECT * FROM " + TABLE_JOKES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst())
        {
            return false ;
        }else{
            return true ;
        }
    }
    // Getting All Jokes
    public List<Joke> getAllJokes() {
        List<Joke> jokeList = new ArrayList<Joke>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_JOKES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Joke joke = new Joke();
                joke.setJokeId(Integer.parseInt(cursor.getString(0)));
                joke.setJokeCategory(cursor.getString(1));
                joke.setJokeTitle(cursor.getString(2));
                joke.setJokeBody(cursor.getString(3));
// Adding contact to list
                jokeList.add(joke);
            } while (cursor.moveToNext());
        }

// return contact list
        return jokeList;
    }
    // Getting jokes Count
    public int getJokesCount() {
        String countQuery = "SELECT * FROM " + TABLE_JOKES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

// return count
        return cursor.getCount();
    }
    // Updating a joke
    public int updateJoke(Joke joke) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, joke.getJokeCategory());
        values.put(KEY_TITLE, joke.getJokeTitle());
        values.put(KEY_BODY,joke.getJokeBody()) ;

// updating row
        return db.update(TABLE_JOKES, values, KEY_ID + " = ?",
                new String[]{String.valueOf(joke.getJokeId())});
    }

    // Deleting a joke
    public void deleteJoke(Joke joke) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_JOKES, KEY_ID + " = ?",
                new String[] { String.valueOf(joke.getJokeId()) });
        db.close();
    }
}
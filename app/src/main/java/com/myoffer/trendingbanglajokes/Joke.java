package com.myoffer.trendingbanglajokes;

/**
 * Created by TahmiD on 5/7/2016.
 */


/**
 * Created by TahmiD on 5/7/2016.
 */
public class Joke {
    private static int joke_id ;
    private static String joke_category ;
    private static String joke_title ;
    private static String joke_body ;

    public Joke(int id, String category, String title, String body){
        joke_id =id ;
        joke_category = category ;
        joke_title = title ;
        joke_body = body ;
    }

    public Joke(String category, String title, String body)
    {
        joke_category = category ;
        joke_title = title ;
        joke_body= body ;
    }

    public Joke(){
        joke_id= 0 ;
        joke_category="" ;
        joke_title = "" ;
    }
    public static int getJokeId()
    {
        return joke_id ;
    }

    public static String getJokeCategory()
    {
        return joke_category ;
    }

    public static String getJokeTitle(){
        return joke_title ;
    }

    public static String getJokeBody(){
        return joke_body ;
    }

    public static String getJokeBody(String body)
    {
        return joke_body ;
    }

    public static void setJokeId(int id)
    {
        joke_id = id ;
    }

    public static void setJokeCategory(String category){
        joke_category = category ;
    }

    public static void setJokeTitle(String title)
    {
        joke_title = title ;
    }

    public static void setJokeBody(String body)
    {
        joke_body = body ;
    }
}

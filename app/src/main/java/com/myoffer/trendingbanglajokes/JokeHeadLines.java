package com.myoffer.trendingbanglajokes;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by TahmiD on 5/9/2016.
 */
public class JokeHeadLines extends AppCompatActivity {

    DBHandler db = new DBHandler(this);
    String CategoryTXT="category not loaded" ;



    public void JokeHeadLines() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {



        super.onCreate(savedInstanceState);
        setContentView(R.layout.headlines_list);

        Bundle receiveBundle = getIntent().getExtras();
        if(receiveBundle!=null)
        CategoryTXT = receiveBundle.getString("value");
        TextView tvCategory = (TextView)findViewById(R.id.tvCategoryName) ;
        LinearLayout llayout = (LinearLayout)findViewById(R.id.llayoutHeadlines) ;
        LinearLayout llayout2 = (LinearLayout)findViewById(R.id.nestedllayoutHeadlines) ;
        int buttonColorId = R.color.buttonColorOffice ;
        int buttonTextId = R.color.buttonTextColorOffice ;

        //llayout.setBackgroundResource(R.drawable.bgoffice) ;
       if(CategoryTXT.equals(getString(R.string.category_office))){
           llayout.setBackgroundResource(R.drawable.bgoffice) ;
          llayout2.setBackgroundResource(R.color.colorOffice);

           buttonColorId = R.color.buttonColorInterview ;
           buttonTextId = R.color.buttonTextColorOffice ;
           tvCategory.setTextColor(getResources().getColor(R.color.buttonColorOffice));

        }
        else if (CategoryTXT.equals(getString(R.string.category_ain_adalot)))
       {
           llayout.setBackgroundResource(R.drawable.bgainadalot);
           llayout2.setBackgroundResource(R.color.colorAinAdalot);
           buttonColorId = R.color.buttonColorAinAdalot ;
           buttonTextId = R.color.buttonTextColorAinAdalot ;
       }
       else if (CategoryTXT.equals(getString(R.string.category_interview)))
       {
           llayout.setBackgroundResource(R.drawable.bginterview);
           llayout2.setBackgroundResource(R.color.colorOffice);
           buttonColorId = R.color.buttonColorInterview ;
           tvCategory.setTextColor(getResources().getColor(R.color.categoryNameAlternate));
       }
       else if (CategoryTXT.equals(getString(R.string.category_ukil)))
       {
           llayout.setBackgroundResource(R.drawable.bgukil);
           llayout2.setBackgroundResource(R.color.colorOffice);
           buttonColorId = R.color.buttonColorInterview ;
           tvCategory.setTextColor(getResources().getColor(R.color.categoryNameAlternate));
       }
       else if (CategoryTXT.equals(getString(R.string.category_kripon)))
       {
           llayout.setBackgroundResource(R.drawable.bgkripon);
           llayout2.setBackgroundResource(R.color.colorKripon);
           buttonColorId = R.color.buttonColorKripon ;
       }
       else if (CategoryTXT.equals(getString(R.string.category_kreta_bikreta)))
       {
           llayout.setBackgroundResource(R.drawable.bgkreta);
           llayout2.setBackgroundResource(R.color.colorOffice);
           buttonColorId = R.color.buttonColorKreta ;
       }
       else if (CategoryTXT.equals(getString(R.string.category_kheladhula)))
       {
           llayout.setBackgroundResource(R.drawable.bgkheladhula);
           llayout2.setBackgroundResource(R.color.colorKheladhula);
           buttonColorId = R.color.buttonColorKheladhula ;

       }
       else if (CategoryTXT.equals(getString(R.string.category_bhritto)))
       {
           llayout.setBackgroundResource(R.drawable.bgbhritto);
           llayout2.setBackgroundResource(R.color.colorAinAdalot);
           buttonColorId=R.color.buttonColorAinAdalot ;
       }
       else if (CategoryTXT.equals(getString(R.string.category_chor)))
       {
           llayout.setBackgroundResource(R.drawable.bgchor);
           llayout2.setBackgroundResource(R.color.colorOffice);
       }
       else if (CategoryTXT.equals(getString(R.string.category_chapabaj)))
       {
           llayout.setBackgroundResource(R.drawable.bgchapabaj);
           llayout2.setBackgroundResource(R.color.colorChapabaz);
           buttonColorId=R.color.buttonColorChapabaz ;

       }

        tvCategory.setText(CategoryTXT);


        final Button headlineButton[] = new Button[10] ;

        headlineButton[0] = (Button)findViewById(R.id.btnHead1) ;
        headlineButton[1]= (Button)findViewById(R.id.btnHead2) ;
        headlineButton[2] = (Button)findViewById(R.id.btnHead3) ;
        headlineButton[3]= (Button)findViewById(R.id.btnHead4) ;
        headlineButton[4] = (Button)findViewById(R.id.btnHead5) ;
        headlineButton[5]= (Button)findViewById(R.id.btnHead6) ;
        headlineButton[6] = (Button)findViewById(R.id.btnHead7) ;
        headlineButton[7]= (Button)findViewById(R.id.btnHead8) ;
        headlineButton[8] = (Button)findViewById(R.id.btnHead9) ;
        headlineButton[9]= (Button)findViewById(R.id.btnHead10) ;

        if(db.isEmpty()==true)readRawTextFile(this,R.raw.raw);

        ArrayList<String> jokesHeadlines = new ArrayList<String>() ;
        jokesHeadlines = db.getAllHeadlines(CategoryTXT) ;

        for( int i = 0; i<jokesHeadlines.size();i++) {
            headlineButton[i].setText(jokesHeadlines.get(i));
            headlineButton[i].setBackgroundResource(buttonColorId);
            headlineButton[i].setTextColor(getResources().getColor(buttonTextId));

        }

        //ON CLICK LISTENERS.....
        headlineButton[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[0].getText());
                startActivity(i);
                
            }
        });


        headlineButton[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[1].getText());
                startActivity(i);
                
            }
        });


        headlineButton[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[2].getText());
                startActivity(i);
                
            }
        });


        headlineButton[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[3].getText());
                startActivity(i);
                
            }
        });


        headlineButton[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[4].getText());
                startActivity(i);
                
            }
        });

        headlineButton[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[5].getText());
                startActivity(i);
                
            }
        });

        headlineButton[6].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[6].getText());
                startActivity(i);
                
            }
        });


        headlineButton[7].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[7].getText());
                startActivity(i);
                
            }
        });


        headlineButton[8].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[8].getText());
                startActivity(i);
                
            }
        });

        headlineButton[9].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(JokeHeadLines.this,JokeBody.class);
                i.putExtra("body",headlineButton[9].getText());
                startActivity(i);
                
            }
        });
    }
/*
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(JokeHeadLines.this,MainActivity.class);
        startActivity(i);
    }
    */
    public void readRawTextFile(Context ctx, int resId) {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while ((line = buffreader.readLine()) != null) {
                final String[] as = line.split("\\|");

                db.addJoke(new Joke(as[0], as[1], as[2]));
            }
        } catch (IOException e) {
            System.out.println("Error");
        }

    }

}

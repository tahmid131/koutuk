package com.myoffer.trendingbanglajokes;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import bolts.AppLinks;

public class MainActivity extends AppCompatActivity{
    DBHandler db = new DBHandler(this);
    String category_name ="" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //txtRawResource.setText(readRawTextFile(this,R.raw.raw));
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);


        Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(this, getIntent());

        if (targetUrl != null) {
            Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
        }
        setContentView(R.layout.activity_main);
        if(db.isEmpty()==true)readRawTextFile(this,R.raw.raw);

      // TextView txtRawResource= (TextView)findViewById(R.id.txtRawResource);

        //txtRawResource.setText(readRawTextFile(this,R.raw.raw));
       


        StringBuilder text = new StringBuilder() ;
// Inserting Joke/Rows

        ImageButton infoImage = (ImageButton)findViewById(R.id.imageButton) ;
        final Button categoryButton[] = new Button[10] ;

        categoryButton[0] = (Button)findViewById(R.id.btnCat1) ;
        categoryButton[1]= (Button)findViewById(R.id.btnCat2) ;
        categoryButton[2] = (Button)findViewById(R.id.btnCat3) ;
        categoryButton[3]= (Button)findViewById(R.id.btnCat4) ;
        categoryButton[4] = (Button)findViewById(R.id.btnCat5) ;
        categoryButton[5]= (Button)findViewById(R.id.btnCat6) ;
        categoryButton[6] = (Button)findViewById(R.id.btnCat7) ;
        categoryButton[7]= (Button)findViewById(R.id.btnCat8) ;
        categoryButton[8] = (Button)findViewById(R.id.btnCat9) ;
        categoryButton[9]= (Button)findViewById(R.id.btnCat10) ;








        ArrayList<String> jokesCategory = new ArrayList<String>() ;
            jokesCategory = db.getAllCategory() ;

        for( int i = 0; i<jokesCategory.size();i++){
            categoryButton[i].setText(jokesCategory.get(i));

        }
        //ON CLICK LISTENERS.....

        infoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeBody.class);
                startActivity(i);

            }
        });
        categoryButton[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_office));
                startActivity(i);

            }
        });


        categoryButton[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_ain_adalot));
                startActivity(i);
                ;
            }
        });


        categoryButton[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_interview));
                startActivity(i);

            }
        });


        categoryButton[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_ukil));
                startActivity(i);

            }
        });


        categoryButton[4].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_kripon));
                startActivity(i);

            }
        });

        categoryButton[5].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_kreta_bikreta));
                startActivity(i);

            }
        });

        categoryButton[6].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_kheladhula));
                startActivity(i);

            }
        });


        categoryButton[7].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_bhritto));
                startActivity(i);

            }
        });


        categoryButton[8].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_chor));
                startActivity(i);
                ;
            }
        });

        categoryButton[9].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,JokeHeadLines.class);
                i.putExtra("value",getString(R.string.category_chapabaj));
                startActivity(i);

            }
        });

    }

      // txtRawResource.setText(db.readAllJokes(text));




        public void readRawTextFile(Context ctx, int resId)
    {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                final String[]  as = line.split("\\|") ;

                db.addJoke(new Joke(as[0],as[1],as[2])) ;
            }
        } catch (IOException e) {
            System.out.println("Error") ;
        }




    }






}

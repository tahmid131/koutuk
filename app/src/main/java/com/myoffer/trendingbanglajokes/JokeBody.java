package com.myoffer.trendingbanglajokes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JokeBody extends AppCompatActivity {
    DBHandler db = new DBHandler(this);
    String jokeBody = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke_body);

        DisplayMetrics dm = new DisplayMetrics() ;
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels ;
        int height = dm.heightPixels ;

        getWindow().setLayout((int)(width*.8), (int)(height*.7));

        Bundle receiveBundle = getIntent().getExtras();
        Button shareButton = (Button)findViewById(R.id.button) ;
        shareButton.setVisibility(View.INVISIBLE);
        TextView tvBody = (TextView) findViewById(R.id.tvBody);
        tvBody.setText("This App is Designed, Developed and Distributed by .... \n\n\n MyOffer360Degree.com") ;
        if (receiveBundle != null) {
            jokeBody = receiveBundle.getString("body");
            if(db.isEmpty()==true)readRawTextFile(this,R.raw.raw);
            final String gotBody =db.getBody(jokeBody) ;
            tvBody.setText(gotBody);
            shareButton.setVisibility(View.VISIBLE);
            shareButton.setOnClickListener(new View.OnClickListener() {
                String S = gotBody ;
                @Override
                public void onClick(View v) {
                    invokeIntent(S);
                }
            });
        }


    }

    public void readRawTextFile(Context ctx, int resId) {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while ((line = buffreader.readLine()) != null) {
                final String[] as = line.split("\\|");

                db.addJoke(new Joke(as[0], as[1], as[2]));
            }
        } catch (IOException e) {
            System.out.println("Error");
        }

    }

    public void invokeIntent(String TXT){

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, TXT);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent,getString(R.string.chooserHead) ));
    }
}

